package com.croco.farm

import android.app.Activity
import android.os.Bundle
import android.widget.Button

class MainActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_plan)
    }

    override fun onStart() {
        super.onStart()
        findViewById<Button>(R.id.btnToKorolev).setOnClickListener {
            startActivity(AndroidLauncher.newIntentWithKorolev(this))
        }
        findViewById<Button>(R.id.btnToKutuzovskiy).setOnClickListener {
            startActivity(AndroidLauncher.newIntentWithKutuzovskiy(this))
        }
        findViewById<Button>(R.id.btnToYangelya).setOnClickListener {
            startActivity(AndroidLauncher.newIntentWithYangelya(this))
        }
        findViewById<Button>(R.id.btnToVDNH).setOnClickListener {
            startActivity(AndroidLauncher.newIntentWithVDNH(this))
        }
    }
}