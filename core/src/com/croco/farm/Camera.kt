package com.croco.farm

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController
import com.badlogic.gdx.graphics.g3d.utils.FirstPersonCameraController
import com.badlogic.gdx.math.Vector3
import com.croco.farm.flat.DOMCLICK_FLAT_HEIGHT

class Camera : LibGdxPart, InputProcessor {

    interface ModeChangeListener {
        fun camerModeChanged(mode: MODE)
    }

    enum class MODE {
        VIEW_FLAT,
        INSIDE_FLAT
    }

    var currentMode: MODE = MODE.VIEW_FLAT
    private var centerOfFlat = Vector3(0f, 0f, 0f)
    var modeChangeListener: ModeChangeListener? = null

    lateinit var cam: PerspectiveCamera
    var camController: InputAdapter? = null

    override fun create() {
        (Gdx.input.inputProcessor as InputMultiplexer).addProcessor(this)
        initCurrentMode()
    }

    private fun newCamera() {
        cam = PerspectiveCamera(67f, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
        cam.near = 1f
        cam.far = 10000f
        cam.update()
    }

    fun setMode(mode: MODE) {
        currentMode = mode
        initCurrentMode()
        modeChangeListener?.camerModeChanged(currentMode)
    }

    private fun initCurrentMode() {
        when (currentMode) {
            MODE.VIEW_FLAT -> {
                newCamera()
                camController = CameraInputController(cam)
                setCentralLook(centerOfFlat)
                cam.lookAt(centerOfFlat.x, centerOfFlat.y, centerOfFlat.z)
                (camController as CameraInputController).target = centerOfFlat
            }
            MODE.INSIDE_FLAT -> {
                newCamera()
                cam.position.set(centerOfFlat.x, DOMCLICK_FLAT_HEIGHT / 2, centerOfFlat.z)
                camController = FirstPersonCameraController(cam)
                (camController as FirstPersonCameraController).setVelocity(100f)
            }
        }
        cam.update()
    }


    fun setCentralLook(look: Vector3) {
        centerOfFlat = look
        cam.position.set(look.x, 300f, look.z)
    }

    override fun render() {
        when (currentMode) {
            MODE.VIEW_FLAT -> {
                (camController as CameraInputController).update()
            }
            MODE.INSIDE_FLAT -> {
                (camController as FirstPersonCameraController).update()
            }
        }
    }

    override fun dispose() {
    }


    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return camController?.touchUp(screenX, screenY, pointer, button) ?: false
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        return camController?.mouseMoved(screenX, screenY) ?: false
    }

    override fun keyTyped(character: Char): Boolean {
        return camController?.keyTyped(character) ?: false
    }

    override fun scrolled(amount: Int): Boolean {
        return camController?.scrolled(amount) ?: false
    }

    override fun keyUp(keycode: Int): Boolean {
        return camController?.keyUp(keycode) ?: false
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        return camController?.touchDragged(screenX, screenY, pointer) ?: false
    }

    override fun keyDown(keycode: Int): Boolean {
        return camController?.keyDown(keycode) ?: false
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return camController?.touchDown(screenX, screenY, pointer, button) ?: false
    }
}