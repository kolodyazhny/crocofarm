package com.croco.farm.jsonmodels

import com.google.gson.annotations.SerializedName

class Plan(
        val id: Int? = null,
        @SerializedName("offer_id")
        val offerId: Int? = null,
        @SerializedName("image_url")
        val imageUrl: String? = null,
        val regions: List<Region>? = null,
        val contours: List<List<List<Int>>>? = null,
        @SerializedName("common_rect")
        val commonRect: CommonRect? = null,
        @SerializedName("created_at")
        val createdAt: String? = null
)