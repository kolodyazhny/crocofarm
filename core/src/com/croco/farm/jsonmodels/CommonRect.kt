package com.croco.farm.jsonmodels

class CommonRect(
    val common: List<List<Int>>? = null,
    val floor: List<List<Int>>? = null,
    val type: String? = null
)