package com.croco.farm.jsonmodels

import com.google.gson.annotations.SerializedName

class Region(
        val rect: List<List<Int>>? = null,
        @SerializedName("class")
        val className: String? = null
)