package com.croco.farm

import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.input.GestureDetector
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.math.collision.BoundingBox
import com.croco.farm.flat.DomclickWall

object GestureListenerImpl : GestureDetector.GestureListener {

    lateinit var walls: List<DomclickWall>
    lateinit var camera: Camera
    var moving = false


    val tapListeners = mutableListOf<GestureDetector.GestureListener>()

    override fun fling(velocityX: Float, velocityY: Float, button: Int): Boolean {
        return false
    }

    override fun zoom(initialDistance: Float, distance: Float): Boolean {
        return false
    }

    override fun pan(x: Float, y: Float, deltaX: Float, deltaY: Float): Boolean {
        return false
    }

    override fun pinchStop() {
    }

    override fun tap(x: Float, y: Float, count: Int, button: Int): Boolean {
        var anyDetected = false
        tapListeners.forEach {
            if (it.tap(x, y, count, button)) {
                anyDetected = true
            }
        }
        return anyDetected
    }

    override fun panStop(x: Float, y: Float, pointer: Int, button: Int): Boolean {
        return false
    }

    override fun longPress(x: Float, y: Float): Boolean {
        if (!moving) {
            detectWallAction(x, y)
        }
        return false
    }

    override fun touchDown(x: Float, y: Float, pointer: Int, button: Int): Boolean {
        var anyDetected = false
        tapListeners.forEach {
            if (it.touchDown(x, y, pointer, button)) {
                anyDetected = true
            }
        }
        return anyDetected
    }

    override fun pinch(initialPointer1: Vector2?, initialPointer2: Vector2?, pointer1: Vector2?, pointer2: Vector2?): Boolean {
        return false
    }


    private fun detectWallAction(screenX: Float, screenY: Float) {

        getObjectPrec(screenX, screenY)
    }

    fun getObject(screenX: Float, screenY: Float) {
        val ray = camera.cam.getPickRay(screenX, screenY)

        var position = Vector3()
        var distance = -1f

        for (i in 0 until walls.size) {
            val bounds = BoundingBox()
            val instance = walls.get(i).instance
            val center = Vector3()
            val dimensions = Vector3()

            instance.calculateBoundingBox(bounds)

            bounds.getCenter(center)
            bounds.getDimensions(dimensions)

            instance.transform.getTranslation(position)
            position.add(center)
            val dist2 = ray.origin.dst2(position)
            if (distance >= 0f && dist2 > distance) continue

            if (Intersector.intersectRayBoundsFast(ray, bounds)) {
                walls.get(i).changeColor()
                break
            }
        }
    }

    fun getObjectPrec(screenX: Float, screenY: Float) {
        val ray = camera.cam.getPickRay(screenX, screenY)

        var position = Vector3()
        var distance = -1f

        for (i in 0 until walls.size) {
            val bounds = BoundingBox()
            val instance = walls.get(i).instance
            val center = Vector3()
            val dimensions = Vector3()

            instance.calculateBoundingBox(bounds)

            bounds.getCenter(center)
            bounds.getDimensions(dimensions)

            instance.transform.getTranslation(position)
            position.add(center)
            val dist2 = ray.origin.dst2(position)
            if (distance >= 0f && dist2 > distance) continue

            if (Intersector.intersectRayBounds(ray, bounds, null)) {
                walls.get(i).changeColor()
                break
            }
        }
    }

}