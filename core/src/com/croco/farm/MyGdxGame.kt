package com.croco.farm

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.input.GestureDetector
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.croco.farm.flat.DomclickFlat
import com.croco.farm.flat.SimpleFlat
import com.croco.farm.hud.MainHUD
import com.croco.farm.jsonmodels.JsonConveter

class MyGdxGame(val json: String? = null) : ApplicationAdapter() {

    var camera = Camera()
    var hud = MainHUD(camera)
    var light = Light()
    var flat = DomclickFlat(json ?: JsonConveter.json)//SimpleFlat()
    var basis = DebugBasis()

    override fun create() {
        val inputMultiplexer = InputMultiplexer()
        Gdx.input.inputProcessor = inputMultiplexer

        light.create()
        camera.create()
        flat.create()
        hud.create()
        basis.create()

        camera.setCentralLook(flat.center())
        camera.setMode(Camera.MODE.VIEW_FLAT)

        val center = flat.center()
        light.environment.add(DirectionalLight().set(1f, 1f, 1f, center.x, center.y, center.z))
        inputMultiplexer.processors.insert(0, GestureDetector(createGestureListener()))

    }

    private fun createGestureListener(): GestureDetector.GestureListener {
        val gestureListener = GestureListenerImpl
        gestureListener.walls = flat.walls
        gestureListener.camera = camera

        return gestureListener
    }

    val rgb = 1f / 255f
    val r = rgb * 135
    val g = rgb * 206
    val b = rgb * 235
    override fun render() {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.width, Gdx.graphics.height)
        Gdx.gl.glClearColor(r, g, b, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)

        camera.render()
        flat.render(camera.cam, light.environment)
        //basis.render(camera.cam, light.environment)
        hud.render()
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        hud.resize(width, height)
    }

    override fun dispose() {
        camera.dispose()
        flat.dispose()
        hud.dispose()
        basis.dispose()
    }
}
