package com.croco.farm

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder


class DebugBasis {

    lateinit var modelBatch: ModelBatch
    lateinit var xLine: ModelInstance //red
    lateinit var yLine: ModelInstance //yellow
    lateinit var zLine: ModelInstance //green

    fun create() {
        modelBatch = ModelBatch()
        createXLine()
        createYLine()
        createZLine()
    }

    private fun createXLine() {
        val modelBuilder = ModelBuilder()
        modelBuilder.begin()
        val builder = modelBuilder.part("line", 1, 3, Material())
        builder.setColor(Color.RED)
        builder.line(1000.0f, 0.0f, 0.0f, -10000.0f, 0.0f, 0.0f)
        val lineModel = modelBuilder.end()
        xLine = ModelInstance(lineModel)
    }

    private fun createYLine() {
        val modelBuilder = ModelBuilder()
        modelBuilder.begin()
        val builder = modelBuilder.part("line", 1, 3, Material())
        builder.setColor(Color.YELLOW)
        builder.line(0.0f, 1000.0f, 0.0f, 0.0f, -1000.0f, 0.0f)
        val lineModel = modelBuilder.end()
        yLine = ModelInstance(lineModel)
    }

    private fun createZLine() {
        val modelBuilder = ModelBuilder()
        modelBuilder.begin()
        val builder = modelBuilder.part("line", 1, 3, Material())
        builder.setColor(Color.GREEN)
        builder.line(0.0f, 0.0f, 1000.0f, 0.0f, 0.0f, -1000.0f)
        val lineModel = modelBuilder.end()
        zLine = ModelInstance(lineModel)
    }

    fun render(cam: PerspectiveCamera, env: Environment) {
        modelBatch.begin(cam)
        modelBatch.render(xLine, env)
        modelBatch.render(yLine, env)
        modelBatch.render(zLine, env)
        modelBatch.end()
    }

    fun dispose() {
        modelBatch.dispose()
        xLine.model.dispose()
        yLine.model.dispose()
        zLine.model.dispose()
    }
}