package com.croco.farm

interface LibGdxPart {
    fun create()
    fun render()
    fun dispose()
}