package com.croco.farm

import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight

class Light : LibGdxPart {

    lateinit var environment: Environment

    override fun create() {
        environment = Environment()
        environment.set(ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f))
       // environment.add(DirectionalLight().set(0.8f, 0.8f, 0.8f, 300f, 0f, 300f))
    }

    override fun render() {
    }

    override fun dispose() {
    }
}