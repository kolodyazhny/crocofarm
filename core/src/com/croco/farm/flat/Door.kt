package com.croco.farm.flat

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.ModelInstance

class Door : FlatModel {

    lateinit var modelBatch: ModelBatch
    var instance: ModelInstance? = null
    lateinit var assets: AssetManager
    var loading: Boolean = false

    override fun create() {
        modelBatch = ModelBatch()
        assets = AssetManager()
        assets.load("door/door.g3db", Model::class.java)
        loading = true
    }

    private fun doneLoading() {
        instance = ModelInstance(assets.get("door/door.g3db", Model::class.java))
        loading = false
    }

    override fun render(cam: PerspectiveCamera, env: Environment) {
        if (loading && assets.update()) {
            doneLoading()
        } else if (instance != null) {
            modelBatch.begin(cam)
            modelBatch.render(instance, env)
            modelBatch.end()
        }
    }

    override fun dispose() {
        modelBatch.dispose()
        assets.dispose()
    }

    override fun getPosition() = instance!!.transform
}