package com.croco.farm.flat

import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.math.Vector3

class SimpleFlat {

    val walls = arrayListOf(Wall(), Wall(), Wall(), Wall())
    val door = Door()

    fun create() {
        walls.forEach { it.create() }

        walls[0].getPosition().setToRotation(Vector3.Y, 90f)

        walls[1].getPosition().translate(0f, 0f, walls[1].w / 2)
        walls[1].getPosition().translate(-walls[1].w / 2, 0f, 0f)

        walls[2].getPosition().translate(0f, 0f, -walls[1].w / 2)
        walls[2].getPosition().translate(-walls[1].w / 2, 0f, 0f)

        walls[3].getPosition().setToRotation(Vector3.Y, 90f)
        walls[3].getPosition().translate(0f, 0f, -walls[1].w)


        door.create()
    }

    fun render(cam: PerspectiveCamera, env: Environment) {
        walls.forEach { it.render(cam, env) }
        door.render(cam, env)
    }

    fun dispose() {
        walls.forEach { it.dispose() }
        door.dispose()
    }


}