package com.croco.farm.flat

import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.math.collision.BoundingBox
import com.croco.farm.jsonmodels.JsonConveter

const val DOMCLICK_FLAT_HEIGHT = 60f

class DomclickFlat(val json: String = JsonConveter.json) {

    val jsonFlat = JsonConveter.getFlatFromJson(json)
    val walls = mutableListOf<DomclickWall>()
    lateinit var floor: DomclickFloor
    val doors = mutableListOf<DomclickDoorSimple>()

    init {
        jsonFlat.plans!![0].contours!!.forEach { coords ->
            walls.add(DomclickWall(coords.map { Vector2(it[0].toFloat(), it[1].toFloat()) }))
        }

        jsonFlat.plans[0].regions!!.forEach { region ->
            if (region.className == "door") {
                doors.add(DomclickDoorSimple(Vector2(region.rect!![0][0].toFloat(), region.rect[0][1].toFloat()),
                        Vector2(region.rect[1][0].toFloat(), region.rect[1][1].toFloat())))
            }
        }

        floor = DomclickFloor(jsonFlat.plans[0].commonRect!!.floor!!.map { Vector2(it[0].toFloat(), it[1].toFloat()) })
    }


    fun create() {
        walls.forEach { it.create() }
        floor.create()
        doors.forEach { it.create() }
    }

    fun render(cam: PerspectiveCamera, env: Environment) {
        walls.forEach { it.render(cam, env) }
        floor.render(cam, env)
        doors.forEach { it.render(cam, env) }
    }

    fun dispose() {
        walls.forEach { it.dispose() }
        floor.dispose()
        doors.forEach { it.dispose() }
    }

    fun center(): Vector3 {
        val box = BoundingBox()
        floor.instance.calculateBoundingBox(box)
        return Vector3(box.centerX, box.centerY, box.centerZ)
    }
}