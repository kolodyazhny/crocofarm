package com.croco.farm.flat

import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.math.Matrix4

interface FlatModel {
    fun create()
    fun render(cam: PerspectiveCamera, env: Environment)
    fun dispose()
    fun getPosition(): Matrix4
}