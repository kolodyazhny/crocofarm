package com.croco.farm.flat

import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.PolygonRegion
import com.badlogic.gdx.graphics.g2d.PolygonSprite
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder
import com.badlogic.gdx.math.EarClippingTriangulator
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute


class DomclickFloor(private val coords: List<Vector2>, private val up: Float = 0f, private val color: Color = Color.GRAY) : FlatModel {

    lateinit var modelBatch: ModelBatch
    lateinit var instance: ModelInstance

    override fun create() {
        modelBatch = ModelBatch()

        val wallTexture = Texture("brick_big.png")
        wallTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat)
        wallTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
        val material = Material(ColorAttribute.createDiffuse(color))
        material.set(IntAttribute(IntAttribute.CullFace, 0))


        val modelBuilder = ModelBuilder()
        modelBuilder.begin()
        val mpb = modelBuilder.part("wall", GL20.GL_TRIANGLES,
                VertexAttributes.Usage.Position.toLong(), material)


        val vertices = FloatArray(coords.size * 2)
        var index = 0
        coords.forEach { v ->
            vertices[index] = v.x
            index++
            vertices[index] = v.y
            index++
        }

        val indices = EarClippingTriangulator().computeTriangles(vertices).items

        val mesh = Mesh(true, coords.size, indices.size, VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"))

        val meshVerices = FloatArray(coords.size * 3)
        index = 0
        coords.forEach { v ->
            meshVerices[index] = v.y
            index++
            meshVerices[index] = up
            index++
            meshVerices[index] = v.x
            index++
            /* meshVerices[index] = 0f
             index++
             meshVerices[index] = 0f
             index++*/
        }
        mesh.setVertices(meshVerices)
        mesh.setIndices(indices)
        mpb.addMesh(mesh)

        instance = ModelInstance(modelBuilder.end())

    }


    override fun render(cam: PerspectiveCamera, env: Environment) {
        modelBatch.begin(cam)
        modelBatch.render(instance, env)
        modelBatch.end()
    }

    override fun dispose() {
        modelBatch.dispose()
    }

    override fun getPosition(): Matrix4 {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    /*
           val vertices = FloatArray(coords.size * 2)
        var index = 0
        coords.forEach { v ->
            vertices[index] = v.x
            index++
            vertices[index] = v.y
            index++
        }
        val pix = Pixmap(1, 1, Pixmap.Format.RGBA8888)
        pix.setColor(0x00FF00AA)
        pix.fill()
        val region = PolygonRegion(TextureRegion(Texture(pix)), vertices,
                EarClippingTriangulator().computeTriangles(vertices).items)
        sprite = PolygonSprite(region)



     */
}