package com.croco.farm.flat

import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder
import com.badlogic.gdx.math.EarClippingTriangulator
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Vector3

class DomclickWall(private val coords: List<Vector2>) : FlatModel {

    val h = DOMCLICK_FLAT_HEIGHT

    val top = DomclickFloor(coords, h, Color(1f / 255f * 211f, 1f / 255f * 211f, 1f / 255f * 211f, 1f))


    lateinit var modelBatch: ModelBatch
    lateinit var instance: ModelInstance

    var wallColorChangedCount = 0

    override fun create() {
        modelBatch = ModelBatch()

        //EarClippingTriangulator().computeTriangles()
        val wallTexture = Texture("brick_big.png")
        wallTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat)
        wallTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
        val material = Material(TextureAttribute.createDiffuse(wallTexture))
        material.set(IntAttribute(IntAttribute.CullFace, 0))

        val modelBuilder = ModelBuilder()
        modelBuilder.begin()
        val mpb = modelBuilder.part(
                "wall",
                GL20.GL_TRIANGLES,
                VertexAttributes.Usage.Position.toLong()
                        or VertexAttributes.Usage.TextureCoordinates.toLong()
                        or VertexAttributes.Usage.Normal.toLong(),
                material)


        for (i in 0 until coords.size - 1) {
            val p1 = coords[i]
            val p2 = coords[i + 1]

            mpb.addMesh(createMesh(p1, p2))

        }
        mpb.addMesh(createMesh(coords[0], coords.last()))

        instance = ModelInstance(modelBuilder.end())
        top.create()

    }

    private fun createMesh(p1: Vector2, p2: Vector2): Mesh {
        val mesh = Mesh(true, 4, 6,
                VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
                VertexAttribute(VertexAttributes.Usage.Normal, 3, ShaderProgram.NORMAL_ATTRIBUTE),
                VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, "a_texCoord0")

        )

//        mesh.setVertices(floatArrayOf(
//                p1.y, 0f, p1.x, 0f, 0f,
//                p1.y, h, p1.x, 0f, 1f,
//                p2.y, h, p2.x, 1f, 1f,
//                p2.y, 0f, p2.x, 1f, 0f)
//        )

        val verts = floatArrayOf(
                p1.y, 0f, p1.x, 0f, 0f, 0f, 0f, 0f,
                p1.y, h, p1.x, 0f, 0f, 0f, 0f, 1f,
                p2.y, h, p2.x, 0f, 0f, 0f, 1f, 1f,
                p2.y, 0f, p2.x, 0f, 0f, 0f, 1f, 0f)


        val indexes = shortArrayOf(0, 1, 2,
                0, 2, 3)

        calcNormals(indexes, verts)

        mesh.setVertices(verts)
        mesh.setIndices(indexes)

        return mesh
    }

    private fun calcNormals(indexes: ShortArray, vertex: FloatArray) {

        val elementSize = 8

        for (i in 0 until indexes.size step 3) {
            val i1 = indexes[i]
            val i2 = indexes[i + 1]
            val i3 = indexes[i + 2]

            val x1 = vertex[i1.toInt() * elementSize]
            val y1 = vertex[i1.toInt() * elementSize + 1]
            val z1 = vertex[i1.toInt() * elementSize + 2]

            val x2 = vertex[i2.toInt() * elementSize]
            val y2 = vertex[i2.toInt() * elementSize + 1]
            val z2 = vertex[i2.toInt() * elementSize + 2]

            val x3 = vertex[i3.toInt() * elementSize]
            val y3 = vertex[i3.toInt() * elementSize + 1]
            val z3 = vertex[i3.toInt() * elementSize + 2]

            // u = p3 - p1
            val ux = x3 - x1
            val uy = y3 - y1
            val uz = z3 - z1

            // v = p2 - p1
            val vx = x2 - x1
            val vy = y2 - y1
            val vz = z2 - z1

            // n = cross(v, u)
            var nx = vy * uz - vz * uy
            var ny = vz * ux - vx * uz
            var nz = vx * uy - vy * ux

            val num2 = nx * nx + ny * ny + nz * nz
            val num = 1f / Math.sqrt(num2.toDouble()).toFloat()
            nx *= num
            ny *= num
            nz *= num

            val numVector = Vector3(nx, ny, nz)
            numVector.nor()
            numVector.rotate(Vector3.Y, 180f)

            vertex[i1.toInt() * elementSize + 3] += numVector.x
            vertex[i1.toInt() * elementSize + 4] += numVector.y
            vertex[i1.toInt() * elementSize + 5] += numVector.z

            vertex[i2.toInt() * elementSize + 3] += numVector.x
            vertex[i2.toInt() * elementSize + 4] += numVector.y
            vertex[i2.toInt() * elementSize + 5] += numVector.z

            vertex[i3.toInt() * elementSize + 3] += numVector.x
            vertex[i3.toInt() * elementSize + 4] += numVector.y
            vertex[i3.toInt() * elementSize + 5] += numVector.z

        }

    }

    override fun render(cam: PerspectiveCamera, env: Environment) {
        modelBatch.begin(cam)
        modelBatch.render(instance, env)
        modelBatch.end()
        top.render(cam, env)
    }

    override fun dispose() {
        modelBatch.dispose()
        top.dispose()
    }

    override fun getPosition(): Matrix4 {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    fun changeColor() {
        wallColorChangedCount++

        val wallTexture: Texture = when (wallColorChangedCount % 4) {
            1 -> Texture("brick_grey.png")
            2 -> Texture("oboi1.png")
            3 -> Texture("domclick_wall.png")
            else -> Texture("brick_big.png")
        }

        wallTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat)
        wallTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
        val mat = Material(TextureAttribute.createDiffuse(wallTexture))
        instance.nodes.get(0).parts.get(0).material.set(mat)

    }

}


/*
*
*   val wallTexture = Texture("brick_big.png")
        wallTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat)
        wallTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
        val material = Material(TextureAttribute.createDiffuse(wallTexture))
        val attrs = VertexAttributes.Usage.Position.toLong() or VertexAttributes.Usage.Normal.toLong() or VertexAttributes.Usage.ColorUnpacked.toLong()


        val modelBuilder = ModelBuilder()
        modelBuilder.begin()
        val mpb = modelBuilder.part("wall", GL20.GL_POINTS,
                attrs, Material())
        mpb.setColor(Color.RED)

        for (i in 0 until coords.size - 1) {
            val p1 = coords[i]
            val p2 = coords[i + 1]

            mpb.rect(p1.y, 0f, p1.x,
                    p1.y, h, p1.x,
                    p2.y, 0f, p2.x,
                    p2.y, h, p2.x,
                    1f, 1f, 1f)
        }
        instance = ModelInstance(modelBuilder.end())

        // mpb.
        // mpb.box(1f, 1f, 1f)*/