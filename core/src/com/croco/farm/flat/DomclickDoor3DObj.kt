package com.croco.farm.flat

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.math.collision.BoundingBox
import net.dermetfan.utils.math.GeometryUtils


class DomclickDoor3DObj(val coord1: Vector2, val coord2: Vector2) : FlatModel {

    lateinit var modelBatch: ModelBatch
    var instance: ModelInstance? = null
    lateinit var assets: AssetManager
    var loading: Boolean = false

    override fun create() {
        modelBatch = ModelBatch()
        assets = AssetManager()
        assets.load("door/door.g3db", Model::class.java)
        loading = true
    }

    private fun doneLoading() {
        instance = ModelInstance(assets.get("door/door.g3db", Model::class.java))


        val box = BoundingBox()
        instance!!.model.calculateBoundingBox(box)
        val scaleHeight = DOMCLICK_FLAT_HEIGHT / box.depth
        val scaleWidth = Vector2.dst(coord1.x, coord1.y, coord2.x, coord2.y) / box.width
        val height = DOMCLICK_FLAT_HEIGHT
        val width = box.width * scaleWidth
        val angle = getAngle()


        val coord3D1 = Vector3(coord1.y, 0f, coord1.x)
        val coord3D2 = Vector3(coord2.y, 0f, coord2.x)
        val doorDir = coord3D2.sub(coord3D1).nor()

        instance!!.transform.translate(coord3D1.x, height / 2, coord3D1.z)

       // doorDir.scl(100f)



        instance!!.transform.rotate(Vector3.X, 90f)

        //instance!!.transform.translate(doorDir.x, 0f, doorDir.y)

        instance!!.transform.rotate(Vector3.Z, -angle.toFloat())


        instance!!.transform.scale(scaleWidth, 1f, scaleHeight)


        //instance!!.transform.setToWorld(Vector3(coord1.y, 0f, coord1.x), Vector3(0f, 0f, 0f), Vector3(0f, 1f, 0f))

        loading = false
    }

    fun getAngle(): Double {
        // v1 moving object

        val coord3D1 = Vector3(coord1.y, 0f, coord1.x)
        val coord3D2 = Vector3(coord2.y, 0f, coord2.x)
        val doorDir = coord3D2.sub(coord3D1).nor()

        val default3D1 = Vector3(0f, 0f, 0f)
        val cdefault3D2 = Vector3(1f, 0f, 0f)
        val defaultDir = cdefault3D2.sub(default3D1).nor()


        val dotProduct = defaultDir.dot(doorDir)
        return Math.toDegrees(Math.acos(dotProduct.toDouble()))
        //  return 2.0 * Math.atan((defaultDir.sub(doorDir)).len().toDouble() / (defaultDir.add(doorDir)).len().toDouble())
    }

    override fun render(cam: PerspectiveCamera, env: Environment) {
        if (loading && assets.update()) {
            doneLoading()
        } else if (instance != null) {
            modelBatch.begin(cam)
            modelBatch.render(instance, env)
            modelBatch.end()
        }
    }

    override fun dispose() {
        modelBatch.dispose()
        assets.dispose()
    }

    override fun getPosition() = instance!!.transform
}