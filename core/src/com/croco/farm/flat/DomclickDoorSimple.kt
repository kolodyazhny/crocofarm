package com.croco.farm.flat

import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2

class DomclickDoorSimple(private val coord1: Vector2,
                         private val coord2: Vector2) : FlatModel {

    val h = DOMCLICK_FLAT_HEIGHT


    lateinit var modelBatch: ModelBatch
    lateinit var instance: ModelInstance

    override fun create() {
        modelBatch = ModelBatch()

        val wallTexture = Texture("door_room.jpeg")
        wallTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat)
        wallTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
        val material = Material(TextureAttribute.createDiffuse(wallTexture))
        material.set(IntAttribute(IntAttribute.CullFace, 0))

        val modelBuilder = ModelBuilder()
        modelBuilder.begin()
        val mpb = modelBuilder.part("door", GL20.GL_TRIANGLES,
                VertexAttributes.Usage.Position.toLong() or VertexAttributes.Usage.TextureCoordinates.toLong(), material)


        val mesh = Mesh(true, 4, 6, VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"),
                VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, "a_texCoord0"))
        mesh.setVertices(floatArrayOf(coord1.y, 0f, coord1.x, 1f,1f,
                coord1.y, h, coord1.x, 1f, 0f,
                coord2.y, h, coord2.x, 0f, 0f,
                coord2.y, 0f, coord2.x, 0f, 1f))
        mesh.setIndices(shortArrayOf(0, 1, 2,
                0, 2, 3))

        mpb.addMesh(mesh)

        instance = ModelInstance(modelBuilder.end())

    }


    override fun render(cam: PerspectiveCamera, env: Environment) {
        modelBatch.begin(cam)
        modelBatch.render(instance, env)
        modelBatch.end()
    }

    override fun dispose() {
        modelBatch.dispose()
    }

    override fun getPosition(): Matrix4 {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

