package com.croco.farm.flat

import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.*
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder

open class Wall(val w: Float = 300f, val h: Float = 40f, val depth: Float = 2f) : FlatModel {

    lateinit var modelBatch: ModelBatch
    lateinit var model: Model
    lateinit var instance: ModelInstance

    override fun create() {
        modelBatch = ModelBatch()

        val wallTexture = Texture("brick_big.png")

        wallTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat)
        wallTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
        val modelBuilder = ModelBuilder()
        model = modelBuilder.createBox(w, h, depth,
                Material(
                        TextureAttribute.createDiffuse(wallTexture)
                ),
                (VertexAttributes.Usage.Position
                        or VertexAttributes.Usage.Normal
                        or VertexAttributes.Usage.TextureCoordinates).toLong())
        instance = ModelInstance(model)
        //instance.transform.setToRotation(Vector3.Z, 120f)
    }

    override fun render(cam: PerspectiveCamera, env: Environment) {
        modelBatch.begin(cam)
        modelBatch.render(instance, env)
        modelBatch.end()
    }

    override fun dispose() {
        model.dispose()
        modelBatch.dispose()
    }

    override fun getPosition() = instance.transform

}