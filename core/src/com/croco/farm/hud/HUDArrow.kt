package com.croco.farm.hud

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.math.collision.BoundingBox
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener

class HUDArrow constructor(val direction: String) : HUDBase {

    companion object {
        const val LEFT = "LEFT"
        const val RIGHT = "RIGHT"
        const val UP = "UP"
        const val DOWN = "DOWN"
    }

    lateinit var texture: Texture
    lateinit var stage: Stage
    lateinit var sprite: Sprite
    lateinit var batch: Batch

    private val arrowSizeInWindowPercent = 0.05f

    private var x = 0f
    private var y = 0f

    private var inited = false

    override fun create() {
        batch = SpriteBatch()
        texture = Texture(if (direction == RIGHT || direction == LEFT) Gdx.files.internal("arrow.png") else Gdx.files.internal("arrow_vertical.png"))
        sprite = Sprite(texture)
        stage = Stage()


        when (direction) {
            RIGHT -> {
            }
            LEFT -> {
                sprite.setFlip(true, false)
            }
            UP -> {
            }
            DOWN -> {
                sprite.setFlip(false, true)
            }
        }
    }

    override fun render() {
        if (!inited) {
            val newHeight = stage.viewport.worldHeight * arrowSizeInWindowPercent
            sprite.scale(newHeight / sprite.height)
            inited = true
        }

        batch.begin()

        when (direction) {
            RIGHT -> {
                x = stage.viewport.worldWidth - sprite.width - 30
                y = 60f
            }
            LEFT -> {
                x = stage.viewport.worldWidth - sprite.width - 140
                y = 60f
            }
            UP -> {
                x = stage.viewport.worldWidth - sprite.width - 85
                y = 110f
            }
            DOWN -> {
                x = stage.viewport.worldWidth - sprite.width - 85
                y = 10f
            }
            else -> {
                x = stage.viewport.worldWidth - sprite.width - 50
                y = 10f
            }
        }
        sprite.x = x
        sprite.y = y
        sprite.draw(batch)
        batch.end()
    }

    override fun resize(w: Int, h: Int) {
        stage.viewport.update(w, h, true)
    }

    override fun dispose() {
        stage.dispose()
    }

    fun checkIntersect(tapX: Float, tapY: Float): Boolean {
        val box = BoundingBox()
        box.set(Vector3(x, y, 0f), Vector3(x + sprite.width, y + sprite.height, 0f))

        return box.contains(Vector3(tapX, stage.viewport.worldHeight - tapY, 0f))
    }

}
