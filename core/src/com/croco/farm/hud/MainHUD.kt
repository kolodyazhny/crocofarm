package com.croco.farm.hud

import com.croco.farm.Camera

class MainHUD(private val cam: Camera) : HUDBase {

    private val hud = arrayListOf(HUDFps(), HUDArrowController(cam), HUDChangeMode(cam))

    override fun create() {
        hud.forEach { it.create() }
    }

    override fun render() {
        hud.forEach { it.render() }
    }

    override fun resize(w: Int, h: Int) {
        hud.forEach { it.resize(w, h) }
    }

    override fun dispose() {
        hud.forEach { it.dispose() }
    }

}