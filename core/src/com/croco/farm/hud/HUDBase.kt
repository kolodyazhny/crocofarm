package com.croco.farm.hud

interface HUDBase {
    fun create()
    fun render()
    fun resize(w: Int, h: Int)
    fun dispose()
}