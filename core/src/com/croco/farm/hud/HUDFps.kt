package com.croco.farm.hud

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.scenes.scene2d.Stage


class HUDFps : HUDBase {
    lateinit var label: Label
    lateinit var font: BitmapFont
    lateinit var stage: Stage

    override fun create() {
        font = BitmapFont()
        label = Label(" ", Label.LabelStyle(font, Color.WHITE))
        stage = Stage()
        stage.addActor(label)
    }

    override fun render() {
        label.setText(" FPS: " + Gdx.graphics.framesPerSecond + "| X:red, Y:yellow, Z:green")
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height, true)
    }

    override fun dispose() {
        stage.dispose()
    }

}