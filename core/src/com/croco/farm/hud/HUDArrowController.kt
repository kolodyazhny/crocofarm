package com.croco.farm.hud

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.graphics.g3d.utils.FirstPersonCameraController
import com.croco.farm.Camera
import com.croco.farm.GestureListenerImpl

class HUDArrowController(private val cam: Camera) : HUDBase, InputProcessor, Camera.ModeChangeListener {

    private val hud = arrayListOf(HUDArrow(HUDArrow.LEFT), HUDArrow(HUDArrow.RIGHT), HUDArrow(HUDArrow.UP), HUDArrow(HUDArrow.DOWN))
    private var visible = true
    private var lastKey: Int? = null

    init {
        cam.modeChangeListener = this
    }

    override fun camerModeChanged(mode: Camera.MODE) {
        visible = mode == Camera.MODE.INSIDE_FLAT
    }

    override fun create() {
        (Gdx.input.inputProcessor as InputMultiplexer).addProcessor(this)
        hud.forEach { it.create() }
    }

    override fun render() {
        if (visible) {
            hud.forEach { it.render() }
        }
    }

    override fun resize(w: Int, h: Int) {
        hud.forEach { it.resize(w, h) }
    }

    override fun dispose() {
        hud.forEach { it.dispose() }
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        if (visible) {
            val direction = checkHud(screenX.toFloat(), screenY.toFloat())
            if (direction != null) {
                when (direction) {
                    HUDArrow.UP -> {
                        lastKey = Input.Keys.W
                        (cam.camController as FirstPersonCameraController).keyDown(Input.Keys.W)
                    }
                    HUDArrow.DOWN -> {
                        lastKey = Input.Keys.S
                        (cam.camController as FirstPersonCameraController).keyDown(Input.Keys.S)
                    }
                    HUDArrow.LEFT -> {
                        lastKey = Input.Keys.A
                        (cam.camController as FirstPersonCameraController).keyDown(Input.Keys.A)

                    }
                    HUDArrow.RIGHT -> {
                        lastKey = Input.Keys.D
                        (cam.camController as FirstPersonCameraController).keyDown(Input.Keys.D)
                    }
                }
                (cam.camController as FirstPersonCameraController).keyDown(lastKey!!)
                (cam.camController as FirstPersonCameraController).update()
                GestureListenerImpl.moving = true
                return true
            }
        }
        return false
    }

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        GestureListenerImpl.moving = false
        if (lastKey != null) {
            (cam.camController as FirstPersonCameraController).keyUp(lastKey!!)
            (cam.camController as FirstPersonCameraController).update()
            lastKey = null
            return true
        }
        return false
    }

    override fun mouseMoved(screenX: Int, screenY: Int) = false
    override fun keyTyped(character: Char) = false
    override fun scrolled(amount: Int) = false

    override fun keyUp(keycode: Int) = false
    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int) = false
    override fun keyDown(keycode: Int) = false


    private fun checkHud(x: Float, y: Float): String? {
        hud.forEach {
            if (it.checkIntersect(x, y)) {
                return it.direction
            }
        }
        return null
    }
}