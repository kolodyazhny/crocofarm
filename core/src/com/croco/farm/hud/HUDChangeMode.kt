package com.croco.farm.hud

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.input.GestureDetector
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.math.collision.BoundingBox
import com.badlogic.gdx.scenes.scene2d.Stage
import com.croco.farm.Camera
import com.croco.farm.GestureListenerImpl

class HUDChangeMode(private val cam: Camera) : HUDBase, GestureDetector.GestureListener {

    lateinit var texture: Texture
    lateinit var stage: Stage
    lateinit var sprite: Sprite
    lateinit var batch: Batch

    private var x = 0f
    private var y = 0f

    init {
        GestureListenerImpl.tapListeners.add(this)
    }

    override fun create() {
        batch = SpriteBatch()
        texture = Texture("eye_icon.png")
        sprite = Sprite(texture)
        stage = Stage()
    }

    override fun render() {
        batch.begin()
        x = stage.viewport.worldWidth - sprite.width - 30
        y = stage.viewport.worldHeight - sprite.height - 20
        batch.draw(sprite, x, y)
        batch.end()
    }

    override fun resize(w: Int, h: Int) {
        stage.viewport.update(w, h, true)
    }

    override fun dispose() {
        stage.dispose()
    }

    override fun tap(tapX: Float, tapY: Float, count: Int, button: Int): Boolean {
        val box = BoundingBox()
        box.set(Vector3(x, y, 0f), Vector3(x + sprite.width, y + sprite.height, 0f))

        if (box.contains(Vector3(tapX, stage.viewport.worldHeight - tapY, 0f))) {
            cam.setMode(if (cam.currentMode == Camera.MODE.VIEW_FLAT) Camera.MODE.INSIDE_FLAT else Camera.MODE.VIEW_FLAT)
            return true
        }
        return false
    }

    override fun fling(velocityX: Float, velocityY: Float, button: Int) = false
    override fun zoom(initialDistance: Float, distance: Float) = false

    override fun pan(x: Float, y: Float, deltaX: Float, deltaY: Float) = false

    override fun pinchStop() {
    }

    override fun panStop(x: Float, y: Float, pointer: Int, button: Int) = false

    override fun longPress(x: Float, y: Float) = false

    override fun touchDown(x: Float, y: Float, pointer: Int, button: Int) = false

    override fun pinch(initialPointer1: Vector2?, initialPointer2: Vector2?, pointer1: Vector2?, pointer2: Vector2?) = false
}